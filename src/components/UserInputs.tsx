import React from 'react';
import styled from 'styled-components';
import { SectionHeaderStyles } from '../styles/SectionHeader';
import { InputStyles } from '../styles/InputStyles';




const InputContainerStyles = styled.div`
  display: flex;
  align-content: space-between;
  align-items: center;
`;

const ImageStyles = styled.img`
  height: 1.2rem;
`;

type UserInputProps = {
  headerText: string;
  input: number;
  min: number;
  img: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
};

const UserInput = ({
  headerText,
  img,
  min,
  input,
  onChange,
}: UserInputProps) => {
  return (
    <>
      <SectionHeaderStyles>{headerText}</SectionHeaderStyles>
      <InputContainerStyles>
        <ImageStyles src={`/images/${img}.svg`} />
        <InputStyles
          type="number"
          min={min}
          onFocus={(e) => e.target.select()}
          onChange={onChange}
          value={input}
        />
      </InputContainerStyles>
    </>
  );
};

export default UserInput;
