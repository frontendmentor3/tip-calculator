import React from 'react';
import styled from 'styled-components';
import { formatMoney } from '../utils/formatMoney';

type TipOutputProps = {
  tip: number;
  total: number;
  person: number;
};

type TipDisplayBlockProps = {
  title: string;
  amount: string;
};

const TipOutputContainer = styled.div`
  grid-area: display; 
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-width: 280px;
  height: 80%;
  padding: 30px;
  border-radius: 20px;
  background-color: var(--very-dark-cyan);
  margin-top: 1.5rem;
`;

const TipOutPutBlockContainer = styled.div`  
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const BlockTitleStyles = styled.div`
  color: var(--white);
`;

const BlockSubtitleStyles = styled.div`
  color: var(--dark-grayish-cyan);
`;

const BlockMoneyStyles = styled.div`
  color: var(--strong-cyan);
`;

const ResetButtonStyles = styled.div`
  color: var(--very-dark-cyan);
  background-color: var(--strong-cyan);
  text-align: center
`;

const TipDisplayBlock = ({ title, amount }: TipDisplayBlockProps) => {
  return (
    <TipOutPutBlockContainer>
      <div>
        <BlockTitleStyles>{title}</BlockTitleStyles>
        <BlockSubtitleStyles>/ person</BlockSubtitleStyles>
      </div>
      <BlockMoneyStyles>${amount}</BlockMoneyStyles>
    </TipOutPutBlockContainer>
  );
};

const TipOutput = ({
  tip: tipPercent,
  total: bill,
  person,
}: TipOutputProps) => {
  const tipPerPerson = (): string => {
    if (bill) {
      return formatMoney((bill * (tipPercent / 100)) / person);
    }
    return '0';
  };

  const totalPerPerson = (): string => {
    if (bill) {
      return formatMoney((bill * (1 + tipPercent / 100)) / person);
    }
    return '0';
  };

  return (
    <TipOutputContainer>
        <div>
          <TipDisplayBlock
            title={'Tip Amount'}
            amount={tipPerPerson()}
          ></TipDisplayBlock>
          <TipDisplayBlock
            title={'Total'}
            amount={totalPerPerson()}
          ></TipDisplayBlock>
        </div>
          <ResetButtonStyles>RESET</ResetButtonStyles>

    </TipOutputContainer>
  );
};

export default TipOutput;
