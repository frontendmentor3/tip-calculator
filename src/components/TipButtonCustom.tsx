import React, { useState } from 'react';
import { InputStyles } from '../styles/InputStyles';

type TipButtonProps = {
  onClick: (percentage: number) => void;
};

const TipButtonCustom = ({ onClick }: TipButtonProps) => {
  const [tipPercent, setTipPercent] = useState(0);

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = parseInt(e.target.value);
    if (!isNaN(value) && value && value >= 0) {
      setTipPercent(value);
      onClick(value);
    } else {
      //TODO: set error message
      if (value < 0) {
        alert('Tip % cannot be negative');
      }
      setTipPercent(0);
      onClick(0);
    }
  };
  return (
    <InputStyles
      placeholder="Custom"
      value={tipPercent === 0 ? ' ' : tipPercent}
      onChange={handleOnChange}
      onFocus={(e) => e.target.select()}
      type="number"
      min="0"
    />
  );
};

export default TipButtonCustom;
