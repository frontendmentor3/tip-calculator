import React from 'react';
import styled from 'styled-components';

type TipButtonProps = {
  percentage: number;
  isActive: boolean;
  onClick: (percentage: number) => void;
};

const TipButtonStyles = styled.div`
  background-color: var(--very-dark-cyan);
  color: var(--white);
  padding: 10px;
  border-radius: 5px;
  text-align: center;
  cursor: pointer;

  &.active {
    background-color: var(--strong-cyan);
    color: var(--very-dark-cyan);
  }
`;

const TipButton = ({ percentage, isActive, onClick }: TipButtonProps) => {
  const handleOnClick = () => {
    onClick(percentage);
  };
  return (
    <TipButtonStyles
      className={isActive ? 'active' : ''}
      onClick={handleOnClick}
    >
      {percentage}%
    </TipButtonStyles>
  );
};

export default TipButton;
