export const formatMoney = (money: number): string => {
  return money.toFixed(2);
};
