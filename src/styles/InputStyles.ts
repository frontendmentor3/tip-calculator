import styled from 'styled-components';
import { fontSize } from './sizes';

export const InputStyles = styled.input`
  background-color: var(--lighter-grayish-cyan);
  color: var(--very-dark-cyan);
  text-align: right;
  font-weight: bold;
  font-family: 'Space Mono', monospace;
  font-size: ${fontSize.primary};
  padding-right: 10px;
  border-radius: 5px;
  border-width: 0px;
  width: auto;

  &:focus {
    outline: none !important;
    border: 3px solid var(--strong-cyan);
  }

  /* Chrome, Safari, Edge, Opera */
  &::-webkit-outer-spin-button,
  &::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  /* Firefox */
  &[type='number'] {
    -moz-appearance: textfield;
  }
`;
