import styled from 'styled-components';
import { fontSize } from './sizes';

export const SectionHeaderStyles = styled.div`
  color: var(--darker-grayish-cyan);
  font-size: ${fontSize.subTitle};
  font-weight: bold;
  margin-top: 30px;
  margin-bottom: 10px;
`;
