export const fontSize = {
  primary: '24px',
  subTitle: '16px',
};

const SCREEN_SIZES = {
  desktop: '800px'
}

export const devices = {
  desktop: `(min-width:${SCREEN_SIZES.desktop})`
}