import { createGlobalStyle } from 'styled-components';
import { fontSize } from './sizes';

export const GlobalStyles = createGlobalStyle`
  html{
    font-family: 'Space Mono', monospace;
    --strong-cyan: hsl(172, 67%, 45%);
    --very-dark-cyan: hsl(183, 100%, 15%);
    --darker-grayish-cyan: hsl(186, 14%, 43%);
    --dark-grayish-cyan: hsl(184, 14%, 56%);
    --light-grayish-cyan: hsl(185, 41%, 84%);
    --lighter-grayish-cyan: hsl(189, 41%, 97%);
    --white: hsl(0, 0%, 100%);

    font-size: ${fontSize.primary} ;

    background-color: var(--light-grayish-cyan);
  }
`;
