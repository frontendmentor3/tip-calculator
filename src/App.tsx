import React from 'react';
import styled from 'styled-components';
import TipButton from './components/TipButton';
import TipButtonCustom from './components/TipButtonCustom';
import './App.css';
import UserInput from './components/UserInputs';
import { SectionHeaderStyles } from './styles/SectionHeader';
import TipOutput from './components/TipOutput';
import { useState } from 'react';
import {devices} from './styles/sizes';
import { GlobalStyles } from './styles/GlobalStyles';

const HeaderStyles = styled.div`
  text-align: center;
  color: var(--darker-grayish-cyan);
  font-weight: 700;
  letter-spacing: 10px;
  margin: 30px 0px;
`;

const AppContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 5%;
  background-color: var(--white);
  border-radius: 30px;
  max-width: 1000px;
  margin: auto;
  
  @media ${devices.desktop}{
    display: grid;
    grid-gap: 20px;
    grid-template-columns: 1fr 1fr;
    grid-template-rows: auto;
    grid-template-areas:
      "amount-input display"
      "select-tip display"
      "number-of-people display"
      "attribute attribute";
  }
`;

const AmountInputStyles = styled.div`
    grid-area: amount-input;
`

const NumberOfPeopleStyles = styled.div`
  grid-area: number-of-people;
`
const TipsSelectionContainerStyles = styled.div`
  grid-area: select-tip;
  max-width: 350px;
`

const ButtonContainerStyles = styled.div`
  
  display: grid;
  grid-template-columns: repeat(2, minmax(0, 1fr));
  grid-gap: 10px;
`;

const AttributeStyles = styled.div`
  font-size: 11px;
  text-align: center;
  color: hsl(228, 45%, 44%);
  grid-area: attribute;
`;

const App = () => {
  const [bill, setBill] = useState<number>(100);
  const [person, setPerson] = useState<number>(1);
  const [tipPercent, setTipPercent] = useState<number>(0);

  const handleBillChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setBill(parseFloat(e.target.value));
  };

  const handlePersonChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value !== '0') {
      setPerson(parseInt(e.target.value));
    }
  };

  const handleOnClick = (percentage: number) => {
    setTipPercent(percentage);
  };

  return (
    <>
      <GlobalStyles />
      <HeaderStyles>
        SPLI
        <br />
        TTER
      </HeaderStyles>
      <AppContainer>
        <AmountInputStyles>
            <UserInput
                headerText="Bill"
                input={bill}
                min={0}
                img="icon-dollar"
                onChange={(e) => handleBillChange(e)}
            ></UserInput>
        </AmountInputStyles>

          <TipsSelectionContainerStyles>
              <SectionHeaderStyles>Select Tip %</SectionHeaderStyles>
              <ButtonContainerStyles className="tip-btn-container">
                  <TipButton
                      onClick={handleOnClick}
                      percentage={5}
                      isActive={tipPercent === 5}
                  ></TipButton>
                  <TipButton
                      onClick={handleOnClick}
                      percentage={10}
                      isActive={tipPercent === 10}
                  ></TipButton>
                  <TipButton
                      onClick={handleOnClick}
                      percentage={15}
                      isActive={tipPercent === 15}
                  ></TipButton>
                  <TipButton
                      onClick={handleOnClick}
                      percentage={25}
                      isActive={tipPercent === 25}
                  ></TipButton>
                  <TipButton
                      onClick={handleOnClick}
                      percentage={50}
                      isActive={tipPercent === 50}
                  ></TipButton>
                  <TipButtonCustom onClick={handleOnClick}></TipButtonCustom>
              </ButtonContainerStyles>
          </TipsSelectionContainerStyles>

          <NumberOfPeopleStyles>
              <UserInput
                  headerText="Number of People"
                  input={person}
                  min={1}
                  img="icon-person"
                  onChange={(e) => handlePersonChange(e)}
              ></UserInput>
          </NumberOfPeopleStyles>




        <TipOutput tip={tipPercent} total={bill} person={person}></TipOutput>

        <AttributeStyles className="attribution">
          Challenge by{' '}
          <a href="https://www.frontendmentor.io?ref=challenge" rel="noopener noreferrer" target="_blank">
            Frontend Mentor
          </a>
          . Coded by <a href="https://github.com/cherylli">CM</a>.
        </AttributeStyles>
      </AppContainer>
    </>
  );
};

export default App;
